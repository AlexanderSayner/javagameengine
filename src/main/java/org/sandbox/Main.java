package org.sandbox;

import org.sandbox.engine.EngineWindow;
import org.sandbox.engine.scenes.SceneType;
import org.sandbox.util.TypeMatcher;

import java.util.Objects;

/**
 * Program entry point
 *
 * @author archon
 * @since 1.0
 */
public class Main {
    /**
     * Entry point
     *
     * @param args engine startup settings
     */
    public static void main(String[] args) {
        System.out.println("Hello world! - Java Game Engine");
        // Prevent early scene loading if there is a scene option in program arguments
        final EngineWindow engineWindow = EngineWindow.instance(false);
        // --scene editor | --scene game | --scene 0 | --scene 1
        boolean scene = false;
        // Applying settings
        for (final String arg : args) {
            // After argument key check its value
            if (scene) {
                // Change scene if possible, using default otherwise
                if (new TypeMatcher().isInteger(arg)) {
                    final SceneType[] values = SceneType.values();
                    final int i = Integer.parseInt(arg);
                    if (i < values.length) {
                        engineWindow.changeScene(values[i]);
                    } else {
                        System.out.println("WARN: Unknown scene '" + arg + "'");
                    }
                } else {
                    try {
                        engineWindow.changeScene(SceneType.valueOf(arg.toUpperCase()));
                    } catch (IllegalArgumentException e) {
                        System.out.println("WARN: Unknown scene '" + arg + "'");
                    }
                }
                scene = false;
            }
            // First make sure a next arg will be a scene type
            if (Objects.equals(arg, "--scene")) {
                scene = true;
            }
        }
        // Make sure a scene is loaded even if there are no scene argument in args
        if (!EngineWindow.instance().isSceneLoaded()) {
            EngineWindow.instance().changeScene(SceneType.EDITOR);
        }
        // Start the engine
        engineWindow.run();
    }
}
