package org.sandbox.engine;

import org.lwjgl.Version;
import org.lwjgl.glfw.GLFWErrorCallback;
import org.lwjgl.glfw.GLFWVidMode;
import org.lwjgl.opengl.GL;
import org.lwjgl.system.MemoryStack;
import org.sandbox.engine.listeners.KeyListener;
import org.sandbox.engine.listeners.MouseListener;
import org.sandbox.engine.scenes.EmptyScene;
import org.sandbox.engine.scenes.Scene;
import org.sandbox.engine.scenes.SceneType;
import org.sandbox.math.FVector;
import org.sandbox.util.SystemTime;

import java.nio.IntBuffer;

import static java.util.Objects.requireNonNull;
import static org.lwjgl.glfw.Callbacks.glfwFreeCallbacks;
import static org.lwjgl.glfw.GLFW.*;
import static org.lwjgl.opengl.GL11.*;
import static org.lwjgl.system.MemoryStack.stackPush;
import static org.lwjgl.system.MemoryUtil.NULL;

/**
 * <a href="https://www.lwjgl.org/guide">...</a>
 * Singleton core engine class
 *
 * @author archon
 * @since 1.0
 */
public class EngineWindow {
    private static EngineWindow ENGINE_WINDOW;
    // The window handle
    private long window;

    // Personal settings
    private final int fullscreen;
    private final int width, height;
    private final String title;

    // Load a default scene initialised by constructor if there is no arguments provided to change the scene
    private Scene activeScene;
    private FVector rgb = new FVector(.1f, .5f, .1f);

    public FVector getRgb() {
        return rgb;
    }

    public void updateRgb(FVector rgb) {
        this.rgb = rgb;
    }

    /**
     * Default engine settings
     */
    private EngineWindow() {
        this(false, 640, 640, "Java OpenGL Game Engine", true);
    }

    /**
     * Option to load scene manually
     *
     * @param bLoadScene should scene load by class initialisation
     */
    private EngineWindow(boolean bLoadScene) {
        this(false, 640, 640, "Java OpenGL Game Engine", bLoadScene);
    }

    /**
     * Personal settings parameters
     * User-friendly constructor
     *
     * @param fullscreen is starting is fullscreen mode
     */
    private EngineWindow(boolean fullscreen, int width, int height, String title, boolean bLoadScene) {
        this(
                fullscreen ? GLFW_TRUE : GLFW_FALSE,
                width,
                height,
                title,
                SceneType.EDITOR,
                bLoadScene
        );
    }

    /**
     * All setting setup
     */
    private EngineWindow(int fullscreen, int width, int height, String title, SceneType activeScene, boolean bLoadScene) {
        this.fullscreen = fullscreen;
        this.width = width;
        this.height = height;
        this.title = title;
        this.activeScene = bLoadScene ? Scene.loadScene(activeScene) : Scene.emptyScene();
    }

    /**
     * Get singleton instance
     *
     * @return initialised object
     */
    public static EngineWindow instance() {
        if (ENGINE_WINDOW == null) {
            ENGINE_WINDOW = new EngineWindow();
        }
        return ENGINE_WINDOW;
    }

    /**
     * Manual option to load scene by initialising engine
     *
     * @param bLoadScene false if manual scene initialisation is needed
     * @return initialised engine
     */
    public static EngineWindow instance(boolean bLoadScene) {
        if (ENGINE_WINDOW == null) {
            ENGINE_WINDOW = new EngineWindow(bLoadScene);
        }
        return ENGINE_WINDOW;
    }

    /**
     * Set editor or game engine scene at start
     * Scene.init() has to be called after GL.createCapabilities();
     *
     * @param type of scene enum
     */
    public void changeScene(SceneType type) {
        System.out.println("Change scene to '" + type.name().toLowerCase() + "'");
        this.activeScene = Scene.loadScene(type);
    }

    /**
     * At runtime, we are already in GL context, so we need to initialise scene right now,
     * not to wait while game loop start
     *
     * @param type of scene enum
     */
    public void changeSceneAtRuntime(SceneType type) {
        changeScene(type);
        this.activeScene.init();
    }

    /**
     * Manual scene managing - check if it has been initialised
     *
     * @return true is scene has been loaded, false otherwise
     */
    public boolean isSceneLoaded() {
        return !(activeScene == null || activeScene instanceof EmptyScene);
    }

    public void run() {
        System.out.println("Hello LWJGL " + Version.getVersion() + "!");

        init();
        loop();

        // Free the window callbacks and destroy the window
        glfwFreeCallbacks(window);
        glfwDestroyWindow(window);

        // Terminate GLFW and free the error callback
        glfwTerminate();
        requireNonNull(
                glfwSetErrorCallback(null)
        ).free();
    }

    private void init() {
        // Set up an error callback. The default implementation
        // will print the error message in System.err.
        GLFWErrorCallback.createPrint(System.err).set();

        // Initialize GLFW. Most GLFW functions will not work before doing this.
        if (!glfwInit())
            throw new IllegalStateException("Unable to initialize GLFW");

        // Configure GLFW
        glfwDefaultWindowHints(); // optional, the current window hints are already the default
        glfwWindowHint(GLFW_VISIBLE, GLFW_FALSE); // the window will stay hidden after creation
        glfwWindowHint(GLFW_RESIZABLE, GLFW_TRUE); // the window will be resizable
        glfwWindowHint(GLFW_MAXIMIZED, fullscreen); // starting in fullscreen mode

        // Create the window
        window = glfwCreateWindow(width, height, title, NULL, NULL);
        if (window == NULL)
            throw new RuntimeException("Failed to create the GLFW window");

        // Set up a key callback. It will be called every time a key is pressed, repeated or released.
        glfwSetKeyCallback(window, (window, key, scancode, action, mods) -> {
            if (key == GLFW_KEY_ESCAPE && action == GLFW_RELEASE)
                glfwSetWindowShouldClose(window, true); // We will detect this in the rendering loop
        });

        // Get the thread stack and push a new frame
        try (MemoryStack stack = stackPush()) {
            IntBuffer pWidth = stack.mallocInt(1); // int*
            IntBuffer pHeight = stack.mallocInt(1); // int*

            // Get the window size passed to glfwCreateWindow
            glfwGetWindowSize(window, pWidth, pHeight);

            // Get the resolution of the primary monitor
            GLFWVidMode vidmode = glfwGetVideoMode(glfwGetPrimaryMonitor());

            // Center the window
            assert vidmode != null;
            glfwSetWindowPos(
                    window,
                    (vidmode.width() - pWidth.get(0)) / 2,
                    (vidmode.height() - pHeight.get(0)) / 2
            );
        } // the stack frame is popped automatically

        // Cursor movement delegate
        try (final var cursorPosCallback
                     = glfwSetCursorPosCallback(window, MouseListener::mousePosCallback)) {
            assert cursorPosCallback != null;
        }
        // Mouse button pressing delegate
        try (final var mouseButtonCallback =
                     glfwSetMouseButtonCallback(window, MouseListener::mouseButtonCallback)) {
            assert mouseButtonCallback != null;
        }
        // Mouse wheel delegate
        try (final var scrollCallback =
                     glfwSetScrollCallback(window, MouseListener::mouseScrollCallback)) {
            assert scrollCallback != null;
        }
        // Keyboard button pressing delegate
        glfwSetKeyCallback(window, KeyListener::keyCallback);

        // Make the OpenGL context current
        glfwMakeContextCurrent(window);
        // Enable v-sync
        glfwSwapInterval(1);

        // Make the window visible
        glfwShowWindow(window);
    }

    private void loop() {
        // This line is critical for LWJGL's interoperation with GLFW's
        // OpenGL context, or any context that is managed externally.
        // LWJGL detects the context that is current in the current thread,
        // creates the GLCapabilities instance and makes the OpenGL
        // bindings available for use.
        GL.createCapabilities();

        // Initialise scene OpenGL elements
        activeScene.init();

        // Set the clear color to the blank pink
        glClearColor(1.0f, 0.0f, 1.0f, 0.0f);//-

        // Get system time for delta time calculation
        float tickStartedTime = SystemTime.getRunningTime();
        float tickEndedTime; // Dedicate memory right before loop
        float deltaTime = -1f; // Is not valid for the first time

        // Run the rendering loop until the user has attempted to close
        // the window or has pressed the ESCAPE key.
        while (!glfwWindowShouldClose(window)) {
            glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT); // clear the framebuffer

            // Only valid values are updated
            if (deltaTime >= 0) {
                activeScene.eventTick(deltaTime);
            }

            glfwSwapBuffers(window); // swap the color buffers

            // Poll for window events. The key callback above will only be
            // invoked during this call.
            glfwPollEvents();

            // Tick delta time calculation
            tickEndedTime = SystemTime.getRunningTime();
            deltaTime = tickEndedTime - tickStartedTime;
            tickStartedTime = tickEndedTime;
            // Set background color
            glClearColor(rgb.getX(), rgb.getY(), rgb.getZ(), 0f);//-
        }
    }
}
