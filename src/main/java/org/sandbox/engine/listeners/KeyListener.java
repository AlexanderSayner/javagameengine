package org.sandbox.engine.listeners;

import static org.lwjgl.glfw.GLFW.GLFW_PRESS;
import static org.lwjgl.glfw.GLFW.GLFW_RELEASE;

/**
 * Singleton class
 *
 * @author archon
 * @since 1.0
 */
public class KeyListener {
    private static KeyListener instance;
    private boolean keyPressed[] = new boolean[350];

    /**
     * Use get() instead
     */
    private KeyListener() {
    }

    /**
     * Get or create
     *
     * @return singleton
     */
    public static KeyListener get() {
        if (instance == null) {
            instance = new KeyListener();
        }
        return instance;
    }

    public static void keyCallback(long window, int key, int scancode, int action, int mods) {
        if (action == GLFW_PRESS) {
            if (key < get().keyPressed.length) {
                get().keyPressed[key] = true;
            }
        } else if (action == GLFW_RELEASE) {
            if (key < get().keyPressed.length) {
                get().keyPressed[key] = false;
            }
        }
    }

    public static boolean isKeyPressed(int keyCode) {
        return get().keyPressed[keyCode];
    }
}
