package org.sandbox.engine.listeners;

import static org.lwjgl.glfw.GLFW.GLFW_PRESS;
import static org.lwjgl.glfw.GLFW.GLFW_RELEASE;

/**
 * Singleton class
 * Use to store delegates core logic for glfw mouse callbacks
 *
 * @author archon
 * @since 1.0
 */
public class MouseListener {
    private static MouseListener instance;
    private double scrollX, scrollY;
    private double xPos, yPos, lastX, lastY;
    private final boolean[] mouseButtonPressed = new boolean[5];
    private boolean isDragging;

    /**
     * Use public method get() instead to get singleton instance
     */
    private MouseListener() {
    }

    /**
     * Get or create the only instance of mouse listener
     *
     * @return the only instance
     */
    public static MouseListener get() {
        if (instance == null) {
            instance = new MouseListener();
        }
        return instance;
    }

    /**
     * Updates position
     * @param window glfw window index
     * @param xPos
     * @param yPos
     */
    public static void mousePosCallback(long window, double xPos, double yPos) {
        get().lastX = get().xPos;
        get().lastY = get().yPos;
        get().xPos = xPos;
        get().yPos = yPos;
        // No dragging for additional mouse buttons
        get().isDragging = get().mouseButtonPressed[0] || get().mouseButtonPressed[1] || get().mouseButtonPressed[2];
    }

    /**
     * LMB - 0
     * RMB - 1
     * Wheel button - 2
     * Backwards (closer) mouse button - 3
     * Forward mouse button - 4
     *
     * @param window glfw window index
     * @param button mouse button index
     * @param action glfw press or release
     * @param mods
     */
    public static void mouseButtonCallback(long window, int button, int action, int mods) {
        if (action == GLFW_PRESS) {
            if (button < get().mouseButtonPressed.length) {
                get().mouseButtonPressed[button] = true;
            }
        } else if (action == GLFW_RELEASE) {
            if (button < get().mouseButtonPressed.length) {
                get().mouseButtonPressed[button] = false;
                get().isDragging = false;
            }
        }
    }

    public static void mouseScrollCallback(long window, double xOffset, double yOffset) {
        get().scrollX = xOffset;
        get().scrollY = yOffset;
    }

    public void endFrame() {
        this.scrollX = 0;
        this.scrollY = 0;
        this.lastX = this.xPos;
        this.lastY = this.yPos;
    }

    public float getX() {
        return (float) this.xPos;
    }

    public float getY() {
        return (float) this.yPos;
    }

    public float getDx() {
        return (float) (this.lastX - this.xPos);
    }

    public float getDy() {
        return (float) (this.lastY - this.yPos);
    }

    public float getScrollX() {
        return (float) this.scrollX;
    }

    public float getScrollY() {
        return (float) this.scrollY;
    }

    public boolean isDragging() {
        return this.isDragging;
    }

    public boolean mouseButtonDown(int button) {
        if (button < this.mouseButtonPressed.length) {
            return this.mouseButtonPressed[button];
        }
        return false;
    }
}
