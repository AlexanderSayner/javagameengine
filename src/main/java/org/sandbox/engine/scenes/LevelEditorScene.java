package org.sandbox.engine.scenes;

import org.lwjgl.BufferUtils;
import org.sandbox.engine.EngineWindow;
import org.sandbox.engine.listeners.KeyListener;
import org.sandbox.math.FVector;
import org.sandbox.util.EngineLogger;

import java.nio.FloatBuffer;
import java.nio.IntBuffer;
import java.util.Arrays;

import static org.lwjgl.glfw.GLFW.GLFW_KEY_F;
import static org.lwjgl.glfw.GLFW.GLFW_KEY_SPACE;
import static org.lwjgl.opengl.GL20.*;
import static org.lwjgl.opengl.GL30.*;

/**
 * @author archon
 * @since 1.0
 */
public class LevelEditorScene extends Scene {
    private boolean bIsSceneChanging = false;
    private float timeToChangeScene = 1f;
    private final String vertexShader = "#version 330 core\n" +
            "layout (location = 0) in vec3 aPos;\n" +
            "layout (location = 1) in vec4 aColor;\n" +
            "\n" +
            "out vec4 fColor;\n" +
            "\n" +
            "void main() {\n" +
            "    fColor=aColor;\n" +
            "    gl_Position=vec4(aPos, 1.0);\n" +
            "}\n";
    private final String fragmentShader = "#version 330 core\n" +
            "\n" +
            "in vec4 fColor;\n" +
            "\n" +
            "out vec4 color;\n" +
            "\n" +
            "void main(){\n" +
            "    color=fColor;\n" +
            "}\n";
    private int vertex, fragment, shaderProgram;

    private final float[] vertexArray = {
            // position              // color
             .5f, -.5f,  .0f,        1f, 0f, 0f, 1f, // Bottom right
            -.5f,  .5f,  .0f,        0f, 1f, 0f, 1f, // Top left
             .5f,  .5f,  .0f,        0f, 0f, 1f, 1f, // Top right
            -.5f, -.5f,  .0f,        1f, 1f, 0f, 1f  // Bottom left
    };

    // Must be in counter-clockwise order
    private final int[] elementArray = {
            /*
                   x     x

                   x     x
             */
            2, 1, 0, // Top right triangle
            0, 1, 3  // Bottom left triangle
    };

    private int vao, vbo, ebo;

    @Override
    public void init() {
        System.out.println("Load editor scene");
        // ========================
        // Compile and link shaders
        // ========================

        // First load and compile the vertex shader
        vertex = glCreateShader(GL_VERTEX_SHADER);
        // Pass the shader source to the GPU
        glShaderSource(vertex, vertexShader);
        glCompileShader(vertex);
        // Compilation error check for vertex shader
        EngineLogger.shaderCompilationErrorProcessing(vertex, "squareVertex.glsl'\n\tVertex shader compilation failed");

        // Compile fragment shader
        fragment = glCreateShader(GL_FRAGMENT_SHADER);
        // Send shader to the GPU
        glShaderSource(fragment, fragmentShader);
        glCompileShader(fragment);
        // Compilation error check for fragment shader
        EngineLogger.shaderCompilationErrorProcessing(fragment, "squareFragment.glsl'\n\tFragment shader compilation failed");

        // Link shaders
        shaderProgram = glCreateProgram();
        glAttachShader(shaderProgram, vertex);
        glAttachShader(shaderProgram, fragment);
        glLinkProgram(shaderProgram);
        // Check for linking errors
        EngineLogger.shaderLinkingErrorProcessing(shaderProgram, "square shader linking failure");

        // ========================================================
        // Generate VAO, VBO and EBO buffer objects and send to GPU
        // ========================================================
        vao = glGenVertexArrays();
        glBindVertexArray(vao);

        // Create a float buffer of vertices
        final FloatBuffer vertexBuffer = BufferUtils.createFloatBuffer(vertexArray.length);
        vertexBuffer.put(vertexArray).flip();

        // Create VBO to upload the vertex buffer
        vbo = glGenBuffers();
        glBindBuffer(GL_ARRAY_BUFFER, vbo);
        glBufferData(GL_ARRAY_BUFFER, vertexBuffer, GL_STATIC_DRAW);

        // Create the indices and upload
        final IntBuffer elementBuffer = BufferUtils.createIntBuffer(elementArray.length);
        elementBuffer.put(elementArray).flip();

        // Create EBO
        ebo = glGenBuffers();
        glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, ebo);
        glBufferData(GL_ELEMENT_ARRAY_BUFFER, elementBuffer, GL_STATIC_DRAW);

        // Add the vertex attribute pointers
        int positionSize = 3;
        int colorSize = 4;
        int floatSizeBytes = 4;
        int vertexSizeBytes = (positionSize + colorSize) * floatSizeBytes;
        glVertexAttribPointer(0, positionSize, GL_FLOAT, false, vertexSizeBytes, 0);
        glEnableVertexAttribArray(0);

        glVertexAttribPointer(1, colorSize, GL_FLOAT, false, vertexSizeBytes, positionSize * floatSizeBytes);
        glEnableVertexAttribArray(1);
    }

    @Override
    public void eventTick(float deltaTime) {
        // Editor input axis mapping events //

        // Space - change scene
        if (!bIsSceneChanging && KeyListener.isKeyPressed(GLFW_KEY_SPACE)) {
            bIsSceneChanging = true;
        }
        if (bIsSceneChanging && timeToChangeScene > 0) {
            timeToChangeScene -= deltaTime;
            final FVector rgb = EngineWindow.instance().getRgb().multiply(1.01f);
            EngineWindow.instance().updateRgb(rgb);
        } else if (bIsSceneChanging) {
            EngineWindow.instance().changeSceneAtRuntime(SceneType.GAME);
        }

        // f - pay respect to fps
        if (KeyListener.isKeyPressed(GLFW_KEY_F)) {
            System.out.println(" " + (int) (1f / deltaTime) + " " + "FPS");
        }

        // Square drawing
        // Bind shader program
        glUseProgram(shaderProgram);
        // Bind the VAO
        glBindVertexArray(vao);

        // Enable the vertex attribute pointers
        glEnableVertexAttribArray(0);
        glEnableVertexAttribArray(1);

        glDrawElements(GL_TRIANGLES, elementArray.length, GL_UNSIGNED_INT, 0);

        // Unbind everything
        glDisableVertexAttribArray(0);
        glDisableVertexAttribArray(1);

        glBindVertexArray(0);

        glUseProgram(0);
    }

    @Override
    public String toString() {
        return "LevelEditorScene{" +
                "bIsSceneChanging=" + bIsSceneChanging +
                ", timeToChangeScene=" + timeToChangeScene +
                ", vertexShader='" + vertexShader + '\'' +
                ", fragmentShader='" + fragmentShader + '\'' +
                ", vertex=" + vertex +
                ", fragment=" + fragment +
                ", shaderProgram=" + shaderProgram +
                ", vertexArray=" + Arrays.toString(vertexArray) +
                ", elementArray=" + Arrays.toString(elementArray) +
                ", vao=" + vao +
                ", vbo=" + vbo +
                ", ebo=" + ebo +
                '}';
    }
}
