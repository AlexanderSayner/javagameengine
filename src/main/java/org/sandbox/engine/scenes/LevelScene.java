package org.sandbox.engine.scenes;

import org.sandbox.engine.EngineWindow;
import org.sandbox.math.FVector;

/**
 * @author archon
 * @since 1.0
 */
public class LevelScene extends Scene {
    @Override
    public void init() {
        System.out.println("Load game scene");
        EngineWindow.instance().updateRgb(new FVector(.7f, .3f, .1f));
    }

    @Override
    public void eventTick(float deltaTime) {
        // Game input action events //
    }
}
