package org.sandbox.engine.scenes;

/**
 * @author archon
 * @since 1.0
 */
public abstract class Scene {
    public abstract void init();

    /**
     * Change scene only in EngineWindow because of invoking eventTick in the game loop
     *
     * @param sceneType scene index
     * @return new instance
     */
    public static Scene loadScene(SceneType sceneType) {
        switch (sceneType) {
            case GAME:
                return new LevelScene();
            case EDITOR:
            default:
                return new LevelEditorScene();
        }
    }

    /**
     * Into the void
     *
     * @return emptiness
     */
    public static Scene emptyScene() {
        return new EmptyScene();
    }

    public abstract void eventTick(float deltaTime);
}

