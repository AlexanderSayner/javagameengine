package org.sandbox.engine.scenes;

/**
 * Engine scene types
 * Caution! Strict order for array index usage
 *
 * @author archon
 * @since 1.0
 */
public enum SceneType {
    EDITOR,
    GAME
}
