package org.sandbox.math;

/**
 * 3D Vector struct
 *
 * @author archon
 * @since 1.0
 */
public class FVector {
    private float x;
    private float y;
    private float z;

    public FVector(float x, float y, float z) {
        this.x = x;
        this.y = y;
        this.z = z;
    }

    public FVector multiply(float number) {
        final FVector fVector = new FVector(x, y, z);
        fVector.x = x * number;
        fVector.y = y * number;
        fVector.z = z * number;
        return fVector;
    }

    public float getX() {
        return x;
    }

    public float getY() {
        return y;
    }

    public float getZ() {
        return z;
    }

}
