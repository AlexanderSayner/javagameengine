package org.sandbox.util;

import static org.lwjgl.opengl.GL11.GL_FALSE;
import static org.lwjgl.opengl.GL20.*;

/**
 * @author archon
 * @since 1.0
 */
public class EngineLogger {
    /**
     * Compilation shader system out for error print
     *
     * @param shader  program
     * @param message log information
     */
    public static void shaderCompilationErrorProcessing(int shader, String message) {
        // Check for errors in compilation
        int success = glGetShaderi(shader, GL_COMPILE_STATUS);
        if (success == GL_FALSE) {
            int len = glGetShaderi(shader, GL_INFO_LOG_LENGTH);
            System.out.println("ERROR: '" + message);
            System.out.println(glGetShaderInfoLog(shader, len));
        }
    }

    /**
     * Linking shader system out for error print
     *
     * @param program identifier
     * @param message log information
     */
    public static void shaderLinkingErrorProcessing(int program, String message) {
        // Check for errors in compilation
        int success = glGetProgrami(program, GL_LINK_STATUS);
        if (success == GL_FALSE) {
            int len = glGetProgrami(program, GL_INFO_LOG_LENGTH);
            System.out.println("ERROR: '" + message);
            System.out.println(glGetProgramInfoLog(program, len));
        }
    }
}
