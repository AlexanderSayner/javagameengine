package org.sandbox.util;

/**
 * Java system time utils
 *
 * @author archon
 * @since 1.0
 */
public class SystemTime {
    // Assigns when program starts
    public static final float startTime = System.nanoTime();

    /**
     * Absolute value
     *
     * @return duration of running program in seconds
     */
    public static float getRunningTime() {
        return (float) ((System.nanoTime() - startTime) * 1E-9);
    }
}
