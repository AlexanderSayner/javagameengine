package org.sandbox.util;

import java.util.regex.Pattern;

/**
 * @author archon
 * @since 1.0
 */
public class TypeMatcher {
    //private final Pattern doublePattern = Pattern.compile("-?\\d+(\\.\\d+)?");
    private final Pattern intPattern = Pattern.compile("[0-9]+");

    public boolean isInteger(String strNum) {
        if (strNum == null) {
            return false;
        }
        return intPattern.matcher(strNum).matches();
    }
}
